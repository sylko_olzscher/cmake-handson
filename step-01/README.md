# README #

This repository conatins all files for the CMake hands-on workshop.

### Generate debug build ###

```bash
cmake -B build-debug -DCMAKE_BUILD_TYPE=DEBUG
cmake --build build-debug
```
### Generate release build ###

```bash
cmake -B build-release -DCMAKE_BUILD_TYPE=RELEASE
cmake --build build-release
```

### Use other generators then make ###
#### Ninja

```bash
cmake -B build-release -DCMAKE_BUILD_TYPE=RELEASE -G Ninja
cmake --build build-release
```

#### CodeBlocks

```bash
cmake -B build-release -DCMAKE_BUILD_TYPE=RELEASE -G CodeBlocks
```
