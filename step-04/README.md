# README #

In this step we will add an option USE_APPROX

### Generate debug build ###

```bash
cmake -B build-debug -DCMAKE_BUILD_TYPE=DEBUG
cmake --build build-debug
```
### Generate release build ###

```bash
cmake -B build-release -DCMAKE_BUILD_TYPE=RELEASE
cmake --build build-release
```

### Usage ###

```bash
./build-release/demo
```