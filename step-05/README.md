# README #

In this step we will use Doxygen to generate an API documentation.

### Prepare Doxygen ###

```bash
mkdir docs && cd docs
doxygen -g
```

See docs/Doxyfile.in in line 61 and 874.

### Generate debug build ###

```bash
cmake -B build-debug -DCMAKE_BUILD_TYPE=DEBUG
cmake --build build-debug
```
### Generate release build ###

```bash
cmake -B build-release -DCMAKE_BUILD_TYPE=RELEASE
cmake --build build-release
```

### Usage ###

```bash
./build-release/demo
```