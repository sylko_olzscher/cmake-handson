#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>

#include <config.h>	//	include from build directory

#if defined(USE_APPROX)
#include <calc.h>
#else
#include <cmath>
#endif

/**
* @brief A simple program that computes the square root of a number
* 
* @param argc 
* @param argv 
* @return square root
*/
int main(int argc, char *argv[]) {
	if (argc < 2) {
		std::cout 
		<< "Usage: " 
		<< argv[0] 
		<< " v"
		<< HandsOn::HandsOn_VERSION_SUFFIX
		<< " number" 
		<< std::endl;
		return 1;
	}
	
	// convert input to double
	const double inputValue = atof(argv[1]);
	
	// calculate square root
#if defined(USE_APPROX)  
	const double outputValue = mysqrt(inputValue);
#else
	const double outputValue = sqrt(inputValue);
#endif
	
	std::cout << "The square root of " << inputValue << " is " << outputValue
	<< std::endl;
    
    return EXIT_SUCCESS;
}